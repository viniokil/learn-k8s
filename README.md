# Requarements installation

```
pip install mkdocs mkdocs-material pygments pymdown-extensions --user
```

For full documentation visit [mkdocs.org](https://mkdocs.org).


## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

## Additional blocs in mardown

[Admonition](https://squidfunk.github.io/mkdocs-material/extensions/admonition/)

## Text on tabs
[SuperFences](https://facelessuser.github.io/pymdown-extensions/extensions/superfences/#tabbed-fences)
