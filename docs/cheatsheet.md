# Шпаргалка по Kubernetes

### Список всех сервисов
```
kubectl get services
```

### Список всех подов
```
kubectl get pods
```

### Наблюдать за списком нод
```
kubectl get nodes -w
```

### Получить информацию об версии
```
kubectl version
```

### Получить информацию об кластере
```
kubectl cluster-info
```

### Получить конфигурацию
```
kubectl config view
```

### Вывести информацию про ноду с названием `{{name}}`
```
kubectl describe node {{name}}
```

### Список текущих pod'ов
```
kubectl get pods
```

### Описание pod'а с названием `{{name}}`
```
kubectl describe pod {{name}}
```

### Список replication controller'ов
```
kubectl get rc
```

### Список replication controller'ов в `{{namespace}}`
```
kubectl get rc --namespace="{{namespace}}"
```

### Описание replication controller'a c названием `{{name}}`
```
kubectl describe rc {{name}}
```

### Список сервисов
```
kubectl get svc
```

### Описание сервиса c названием `{{name}}`
```
kubectl describe svc {{name}}
```

### Запустить pod под названием `{{name}}`
```
kubectl run {{name}} --image={{image-name}}
```

### Создать описанный сервис из файла `{{manifest.yaml}}`
```
kubectl create -f {{manifest.yaml}}
```

### Масштабировать replication controller с названием `{{name}}` до количества `{{count}}` экземпляров
```
kubectl scale --replicas={{count}} rc {{name}}
```

### Подключить внешний порт `{{external}}` к внутреннему порту `{{internal}}` в replication controller с названием `{{name}}`
```
kubectl expose rc {{name}} --port={{external}} --target-port={{internal}}
```

### Удалить pod с названием `{{name}}`
```
kubectl delete pod {{name}}
```

### Удалить replication controller с названием `{{name}}`
```
kubectl delete rc {{name}}
```

### Удалить сервис с названием `{{name}}`
```
kubectl delete svc {{name}}
```

### Остановить все поды на ноде `{{node}}`
```
kubectl drain {{node}} --delete-local-data --force --ignore-daemonsets
```

### Удалить ноду `{{node}}` из кластера
```
kubectl delete node {{name}}
```

### Выполнить команду `{{command}}` в сервисе `{{service}}`, опционально выбрав контейнер `{{container}}`
```
kubectl exec {{service}} {{command}} [-c {{container}}]
```

### Вывести логи из сервиса с названием `{{name}}`, опционально выбрав контейнер `{{container}}`
```
kubectl logs -f {{name}} [-c {{container}}]
```

### Наблюдать за логами Kublet

```
watch -n 2 cat /var/log/kublet.log
```

### Показать метрики всех нод
```
kubectl top node
```

### Показать метрики для всех pod'ов
```
kubectl top pod
```

### Инициализировать master ноду
```
kubeadm init
```

### Присоединить ноду в кластер Kubernetes
```
kubeadm join --token {{token}} {{master-ip}}:{{master-port}}
```

### Создать namespace с названием `{{name}}`
```
kubectl create namespace {{namespace}}
```

### Разрешить Kubernetes master нодам запускать поды
```
kubectl taint nodes --all node-role.kubernetes.io/master-
```

### Сброс текущего состояния
```
kubeadm reset
```

### Список всех секретов
```
kubectl get secrets
```

### Ссылка на оригинал
[gist.github.com/DanielBerman](https://gist.github.com/DanielBerman/0724195d977f97d68fc2c7bc4a4e0419)
