# k3s

Это полностью совместимый дистрибутив Kubernetes со следующими улучшениями:

- Тяжелую базу данных etcd заменили на встроенный SQLite. Также поддерживаються внешние хранилища данных, такие как PostgreSQL, MySQL и т.д.
- Работа всех компонентов плоскости управления Kubernetes заключена в единый двоичный файл и процесс. 
- Удалена поддержка облачных провайдеров и плагинов для persistent volumes.
- Внешние зависимости были минимизированы (требуется только современное ядро ​​и поддержка cgroup).

Для пакетов K3s требуются зависимости, в том числе:
- containerd
- Flannel
- CoreDNS
- Утилиты на хостовой машине (iptables, socat и т. Д.)

Из коробки работают: 
- локальный провайдер хранилищ
- балансировщик нагрузки для сервисов,
- Helm controller 
- Ingress - Traefik.

## Установка

K3s предоставляет простой скрипт, который установит k3s в качестве службы в системах на основе systemd или openrc.

```tab="Command"
curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644
```

```tab="Output"
[INFO]  Finding latest release
[INFO]  Using v1.0.0 as release
[INFO]  Downloading hash https://github.com/rancher/k3s/releases/download/v1.0.0/sha256sum-amd64.txt
[INFO]  Downloading binary https://github.com/rancher/k3s/releases/download/v1.0.0/k3s
[INFO]  Verifying binary download
[INFO]  Installing k3s to /usr/local/bin/k3s
[INFO]  Skipping /usr/local/bin/kubectl symlink to k3s, already exists
[INFO]  Creating /usr/local/bin/crictl symlink to k3s
[INFO]  Skipping /usr/local/bin/ctr symlink to k3s, command exists in PATH at /usr/bin/ctr
[INFO]  Creating killall script /usr/local/bin/k3s-killall.sh
[INFO]  Creating uninstall script /usr/local/bin/k3s-uninstall.sh
[INFO]  env: Creating environment file /etc/systemd/system/k3s.service.env
[INFO]  systemd: Creating service file /etc/systemd/system/k3s.service
[INFO]  systemd: Enabling k3s unit
Created symlink /etc/systemd/system/multi-user.target.wants/k3s.service → /etc/systemd/system/k3s.service.
[INFO]  systemd: Starting k3s
```

Служба K3s будет настроена на автоматический перезапуск после перезагрузки узла или в случае сбоя или остановки процесса
Дополнительно будут установлены утилиты
- `kubectl`
- `crictl`
- `ctr`
- `k3s-killall.sh`
- `k3s-uninstall.sh`

Файл kubeconfig будет записан, `/etc/rancher/k3s/k3s.yaml` и `kubectl`, установленный K3s, автоматически использует его.

### Проверка состояния сервиса

```tab="Command"
service k3s status
```

```tab="Output"
k3s.service - Lightweight Kubernetes
   Loaded: loaded (/etc/systemd/system/k3s.service; enabled; vendor preset: enabled)
   Active: active (running) since Sun 2019-12-08 16:30:30 EET; 20min ago
     Docs: https://k3s.io
  Process: 9978 ExecStartPre=/sbin/modprobe overlay (code=exited, status=0/SUCCESS)
  Process: 9973 ExecStartPre=/sbin/modprobe br_netfilter (code=exited, status=0/SUCCESS)
 Main PID: 9982 (k3s-server)
    Tasks: 14
   CGroup: /system.slice/k3s.service
           ├─ 9982 /usr/local/bin/k3s server --write-kubeconfig-mode 644
           └─10089 containerd -c /var/lib/rancher/k3s/agent/etc/containerd/config.toml -a /run/k3s/containerd/containerd
```

### Получить список нод

```tab="Command"
k3s kubectl get node
```

```tab="Output"
NAME          STATUS   ROLES    AGE   VERSION
vinz-hp-450   Ready    master   21m   v1.16.3-k3s.2
```

## Удаление

```tab="Command"
k3s-uninstall.sh
```

```tab="Output"
+ id -u
+ [ 1000 -eq 0 ]
+ exec sudo /usr/local/bin/k3s-uninstall.sh
+ id -u
+ [ 0 -eq 0 ]
+ /usr/local/bin/k3s-killall.sh
+ [ -s /etc/systemd/system/k3s.service ]
+ basename /etc/systemd/system/k3s.service
+ systemctl stop k3s.service
+ [ -x /etc/init.d/k3s* ]
+ killtree
+ kill -9
+ do_unmount /run/k3s
+ do_unmount /var/lib/rancher/k3s
+ do_unmount /var/lib/kubelet/pods
+ do_unmount /run/netns/cni-
+ ip link show
+ grep master cni0
+ read ignore iface ignore
+ ip link delete cni0
Cannot find device "cni0"
+ ip link delete flannel.1
Cannot find device "flannel.1"
+ rm -rf /var/lib/cni/
+ iptables-save
+ grep -v KUBE-
+ grep -v CNI-
+ iptables-restore
+ which systemctl
/bin/systemctl
+ systemctl disable k3s
Removed /etc/systemd/system/multi-user.target.wants/k3s.service.
+ systemctl reset-failed k3s
+ systemctl daemon-reload
+ which rc-update
+ rm -f /etc/systemd/system/k3s.service
+ rm -f /etc/systemd/system/k3s.service.env
+ trap remove_uninstall EXIT
+ [ -L /usr/local/bin/kubectl ]
+ [ -L /usr/local/bin/crictl ]
+ rm -f /usr/local/bin/crictl
+ [ -L /usr/local/bin/ctr ]
+ rm -rf /etc/rancher/k3s
+ rm -rf /var/lib/rancher/k3s
+ rm -rf /var/lib/kubelet
+ rm -f /usr/local/bin/k3s
+ rm -f /usr/local/bin/k3s-killall.sh
+ remove_uninstall
+ rm -f /usr/local/bin/k3s-uninstall.sh
```

## Полезные ссылки

- [Офицальная документация](https://rancher.com/docs/k3s/latest/en/)