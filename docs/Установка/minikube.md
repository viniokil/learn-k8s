# Minikube
Официальный сайт [minikube.sigs.k8s.io](https://minikube.sigs.k8s.io)

Самый популярный и простой способ запустить и поработать с k8s.

Поддерживаються все популярные операционные системы:
- Linux
- MacOS
- Windows

Ссылка на инструкцию по установке для всех OC:
https://kubernetes.io/docs/tasks/tools/install-minikube/

### Возможности Minikube
- DNS
- NodePorts
- ConfigMap'ы и Secret'ы
- Dashboard
- Container Runtime: Docker, CRI-O, и containerd
- Включение CNI (Container Network Interface)
- Ingress

!!! info "Все операции проводились на ноутбуке с такой конфигурацией"
    - Linux Ubuntu 18.04
    - CPU 2 ядра, 4 потока
    - RAM 12 GB


## Подготовка

### Проверка поддержки виртуализации
```tab="Command"
grep -E --color 'vmx|svm' /proc/cpuinfo
```

Убедитесь, что выходные данные не пусты.

### Установка virtualbox
!!! info "Официальная документация по установке"
    [https://www.virtualbox.org/wiki/Linux_Downloads](https://www.virtualbox.org/wiki/Linux_Downloads)

Установим из рипозиториев Ubuntu

```tab="Command"
sudo apt install -y virtualbox virtualbox-ext-pack
```

### Установка kubectl
Cкачиваем последнюю версию kubectl

```tab="Command"
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
```

```tab="Output" 
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 44.5M  100 44.5M    0     0  4114k      0  0:00:11  0:00:11 --:--:-- 3707k

```

Делаем файл `kubectl` исполняемым

```tab="Command"
chmod +x ./kubectl
```

Перемещаем kubectl в директорию с исполняемыми файлами

```tab="Command"
sudo mv kubectl /usr/local/bin/kubectl
```

Проверяем успешность установки

```tab="Command"
kubectl version
```

```tab="Output"
Client Version: version.Info{Major:"1", Minor:"16", GitVersion:"v1.16.3", 
GitCommit:"b3cbbae08ec52a7fc73d334838e18d17e8512749", GitTreeState:"clean",
BuildDate:"2019-11-13T11:23:11Z", GoVersion:"go1.12.12", Compiler:"gc", Platform:"linux/amd64"}
```

Исходя из значение `GitVersion` - у нас установлена версия `v1.16.3`

### Установка Minikube 
Скачиваем Minikube из официального github репозитория 
и перемещаем файл в папку с исполняемыми файлами

```tab="Command"
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
chmod +x minikube
sudo mv minikube /usr/local/bin/
```

Проверяем установку minikube

```tab="Command"
minikube version
```

```tab="Output"
minikube version: v1.5.2
commit: 792dbf92a1de583fcee76f8791cff12e0c9440ad-dirty
```

## Запуск minikube
```tab="Command"
minikube start
```

```tab="Output"
😄  minikube v1.5.2 on Debian buster/sid
✨  Automatically selected the 'virtualbox' driver (alternates: [none])
💿  Downloading VM boot image ...
    > minikube-v1.5.1.iso.sha256: 65 B / 65 B [--------------] 100.00% ? p/s 0s
    > minikube-v1.5.1.iso: 143.76 MiB / 143.76 MiB [-] 100.00% 5.50 MiB p/s 27s
🔥  Creating virtualbox VM (CPUs=2, Memory=2000MB, Disk=20000MB) ...
E1207 17:49:56.243020   22255 cache_images.go:80] CacheImage gcr.io/k8s-minikube/storage-provisioner:v1.8.1 -> /home/vinz/.minikube/cache/images/gcr.io/k8s-minikube/storage-provisioner_v1.8.1 failed: fetching image: Get https://gcr.io/v2/: dial tcp 178.150.167.235:443: connect: connection refused
🐳  Preparing Kubernetes v1.16.2 on Docker '18.09.9' ...
E1207 17:50:56.746940   22255 start.go:799] Error caching images:  Caching images for kubeadm: caching images: caching image /home/vinz/.minikube/cache/images/gcr.io/k8s-minikube/storage-provisioner_v1.8.1: fetching image: Get https://gcr.io/v2/: dial tcp 178.150.167.235:443: connect: connection refused
❌  Unable to load cached images: loading cached images: loading image /home/vinz/.minikube/cache/images/gcr.io/k8s-minikube/storage-provisioner_v1.8.1: stat /home/vinz/.minikube/cache/images/gcr.io/k8s-minikube/storage-provisioner_v1.8.1: no such file or directory
💾  Downloading kubeadm v1.16.2
💾  Downloading kubelet v1.16.2
🚜  Pulling images ...
🚀  Launching Kubernetes ... 
⌛  Waiting for: apiserver
🏄  Done! kubectl is now configured to use "minikube"
```

### Просмотр состояния кластера Kubernetes в Minikube

```Command tab=
minikube status
```

```Output tab=
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

### Просмотр состояния кластера Kubernetes в `kubectl`

```Command tab=
kubectl cluster-info
```

```Output tab=
kubectl cluster-info

Kubernetes master is running at https://192.168.99.100:8443
KubeDNS is running at https://192.168.99.100:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

### Получить доступ к Kubernetes Dashboard

```Command tab=
minikube dashboard
```

```Output tab=
🔌  Enabling dashboard ...
🤔  Verifying dashboard health ...
🚀  Launching proxy ...
🤔  Verifying proxy health ...
🎉  Opening http://127.0.0.1:38561/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
Opening in existing browser session.
```

После чего в браузере откроеться Kubernetes Dashboard

### Готово

С этого момента у нас есть базовый кластер Kubernetes, запущенный в виртуальной машине
VirtualBox, которым управляем Minikube.
Для управления используем `kubectl`.

### Остановка кластера

```Command tab=
minikube stop
```

```Output tab=
✋  Stopping "minikube" in virtualbox ...
=🛑  "minikube" stopped.
```

---

## Удаление Minikube

- Удаляем кластер Minikube командной 

```Command tab=
minikube delete
```

- Удаляем двоичный файл с помощью 

```Command tab=
sudo rm /usr/local/bin/minikube
```

- Удалить каталог, содержащий конфигурацию Minikube
```Command tab=
rm -rf ~/.minikube
```

---

## Полезные ссылки

- [Офицальная документация](https://minikube.sigs.k8s.io/docs/)
- [Построение обучающего окржения при помощи Minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/)